**Update 10/18: The deadline for submission is 9AM EST Thursday, 10/20. **

# Cornell Trading Competition

Welcome to Apifiny Algo, the platform with the highest performance modular tools for direct market data access, order execution, research and strategy framework for trading algo implementation! Please see below to get started right away. For more support or to learn more about our platform, please visit our website (https://www.apifiny.com/product/algo) and join our Discord channel (https://discord.gg/qQ9yRvTK2R).

Follow us on social media for exciting new product announcements:

LinkedIn: https://www.linkedin.com/company/apifiny1/

Twitter: https://twitter.com/apifinycrypto

Telegram: https://t.me/ApifinyCommunity

## Registration

Once you register on the official CTC site (https://cornellquantfund.org/competition), an account and password will be created for you on our server. This account will be necessary to access our server for the submission of your strategies at the end of the competition, and optionally, running simulations for your strategies.

**Update 10/11:** Team submissions are welcome, but if you would like individual accounts for your team members, please send their email addresses to info@apifiny.com.

## General details

1. For this competition, you will use the data provided by the Apifiny team to train your strategy on the Apifiny Algo platform.

2. The data for this competition is provided as if it came from a fictional exchange called TOUCOIN, with a fictional trading pair FXR/LIEN. We have provided both SWAP and SPOT data for this pair over a 2 month period. The SPOT data is from 2020-05-01 to 2020-06-30 and the SWAP data is from 2020-07-15 to 2020-09-14.

3. Your strategy must trade ONLY using SPOT, however you are provided with SWAP to use as part of predictions should you choose to.

4. Once your strategy has been submitted, the Apifiny team will test it using 2-month out-of-sample market data. Strategies will be ranked solely on the cumulative PnL.

5. The maker fee is 0.0000 and the taker fee is 0.0004.

## Setting up Apifiny Algo

### Using your own local computer

#### A. Docker (recommended method)

> Please also check out this tutorial video for step by step instructions: https://drive.google.com/file/d/1oe-8jDPvglVoZ69Ov43k4fQo0r8PoX3w

1. Download and run [Docker Desktop](https://docs.docker.com/get-docker/)

2. Run the Algo SDK docker image on a terminal:
   ```
   docker run -v CTCLocalVol:/home/dev -p 5555:5555 -it apifinyalgo/ctc
   ```
   "apifinyalgo/ctc" is the image that Docker will run. Before running it for the first time, Docker will download the image from our server. Once downloaded on your local Docker, the image can be run without going through the download process again.

   The “-v CTCLocalVol:/home/dev” parameter will make Docker persistent so you do not lose your work by creating and attaching a local volume named “CTCLocalVol” to your container. If you need to start over with new files, you can rename this volume in the command.

   You may need to use ***sudo*** here, depending on OS.

   >**${ALGO__HOME}**
   >
   >Your ALGO_HOME for this docker image is /data/cc/algo_sdk. The environment is already preconfigured for you

3. Sync to the latest version:
   ```
   cd ~
   git pull
   ```
   This step will synchronize your local image with the latest version on the git server.
   

4. Load the Jupyter GUI. This allows easier editing of the JSON files than using the terminal. If you prefer to work directly on the terminal, you can skip this step. 

   A. Run the command  with an arbitrary MYTOKEN (i.e. password) that you provide:

   ```
   jupyter lab --allow-root --ip=0.0.0.0 --port=5555 --NotebookApp.token='MYTOKEN' --NotebookApp.iopub_data_rate_limit=10000000000 --notebook-dir /home/dev
   ```

   B. Go to a web browser and visit the site:
   ```
   localhost:5555/?token=MYTOKEN
   ```

   There, you'll see your Jupyter notebook! You can edit C++ files and also pull up a terminal! Jupyter will load the /home/dev folder as the root folder. You can find your strategy files are in there. To use the terminal within Jupyter, go to File > New > Terminal. To return to the terminal outside Jupyter, you need to go to File > Shutdown.

5. Get started! See the "Writing your code" section for more details.

6. To exit Docker, run ```exit``` on the terminal.


#### B. Ubuntu 20.04

1. To download Algo:
   ```
   curl https://algoserver.apifiny.com/static/download/release/algo_sdk_1.2.4.tar.gz -o algo_sdk_1.2.4.tar.gz
   tar -zxvf algo_sdk_1.2.4.tar.gz
   ```

2. Fetch our data via rsync ```rsync -av -e 'ssh -p 2023' username@38.142.207.130:/prod/data/toucoin /prod/data/``` where userid is the account id provided to you when you registered for the competition. Provide your given password when prompted.

3. For a quickstart, clone this repository
   ``` git clone https://gitlab.com/apifiny-algo/cornell-trading-competition.git ```

4. Set up your environment as provided for in "Using our server", steps 3 and beyond.

5. Get started! See the "Writing your code" section for more details.

### C. Using our server

> Please also check out this tutorial video for step by step instructions: https://drive.google.com/file/d/1Twj_7bs8vKAXzzVmUyvtuhkOIHoYgJr5

Competitors are invited to book a 30-min session on our 64-core server to help run simulations via this page: https://calendly.com/apifiny-bd/ctc
Please book only one session per competitor and adhere to the chosen timeslot. If you would like additional time, 12AM to 7AM on weeknights are open hours that can be used without a reservation. Please limit each session to 30 minutes. Usage will be logged.

1. To login, type into a terminal window, updating "userid" with the account id provided to you when you registered for the competition. Provide your given password when prompted: 
   ```
   ssh -p 2023 userid@38.142.207.130
   ```

2. In the home folder (use the ```ls``` command to show its contents), you will see a folder labeled "submission". Each competitor will have a copy of this in their home folders, and these will be used to submit code.

3. To set up the environment, run the following commands, replacing ALGO_HOME with the path to your algo_sdk (on our server, it is at /prod/algo_sdk, so no need to change this for this step):

   ```
   export ALGO_HOME=/prod/algo_sdk
   ```
   ```
   export TZ=UTC
   export LD_LIBRARY_PATH=${ALGO_HOME}/bin:$LD_LIBRARY_PATH
   export PATH=${ALGO_HOME}/bin:$PATH
   export PATH=${ALGO_HOME}/scripts:$PATH
   export PYTHONPATH=${ALGO_HOME}/scripts:$PYTHONPATH

   export PATH=${HOME}/bin:$PATH
   ```

   You may wish to save this to your ~/.bashrc

4. Go into cornell-trading-competition folder. The full path is ~/cornell-trading-competition.

5. There is a default configuration file provided, maker.json in the examples folder. The full path is ~/cornell-trading-competition/examples. To run it, run the command, updating the date in YYYYMMDD format: 
   ```
   ccc_sim_trader examples/maker.json 20200630
   ```
  
   >**To run a strategy you already built on your local computer**: 
   >
   >A. Place your C++ code (if applicable) and your configuration file into one folder **using the terminal for your local computer via Jupyter**. We will assume you will name the folder dir1. If you worked directly in the "examples" folder, you can copy this folder to "dir1" by running this command from the /home/dev folder: ```cp -r examples dir1```.
   >
   >B. Run the following command **on the terminal for your local computer via Jupyter** to copy the folder from your computer to the server, updating the userid with your own: 
   >   ```
	>  rsync -a -e "ssh -p 2023" dir1 userid@38.142.207.130:~/cornell-trading-competition/
	>  ```
   >
   >C. To run the strategy, go back to the ~/cornell-trading-competition folder **on the server terminal**, and continue the steps below, updating the path from examples to dir1.

5. To run your strategy over a date range, run the following command from the cornell-trading-competition folder, updating the dates in YYYYMMDD format and the path in the last parameter:

    ```
	gen_dates.py -sd 20200501 -ed 20200630 | parallel -j 64 ccc_sim_trader examples/maker.json
	```

6. To get summary statistics of how your strategy did, run the following command from the cornell-trading-competition folder, updating the dates in YYYYMMDD format:

   ```
   sim_ana.py -sd 20200501 -ed 20200630 -p logs
   ```
>**To run your strategy using SWAP data instead,** replace maker.json with maker-swap.json and update the dates in the commands above. The SPOT data is from 2020-05-01 to 2020-06-30 and the SWAP data is from 2020-07-15 to 2020-09-14.

## Writing your code

### Editing the JSON files

> Please also check out this tutorial video for step by step instructions: https://drive.google.com/file/d/1lAZ5DIzrj-dkJV9nYPph7sHk_y5dFqot/

For detailed documentation, visit https://algo.apifiny.com/user_guide/configuration/. In general terms, our platform uses json files to configure trading applications, namely maker.json and maker-swap.json in the “examples” folder. We do not recommend editing any part of the JSON file above the Variables section unless you are familiar and have a specific reason. In most cases, it is sufficent to leave that part as is.

1. You can find the sample json configuration files in the examples folder. The full path is /home/dev/examples. 

2. To edit the JSON files on Jupyter, right click on the file in the left file directory and choose Open With Editor.

### Configuring the strategy components

> For context, you can find the trade data in spreadsheet format here: https://docs.google.com/spreadsheets/d/1yaf8DI84BJMubyVt03sGeo-50F5hj4W5OqMXy2f7GpM
> 
> Please also check out this tutorial video for instructions on analyzing your logs: https://drive.google.com/file/d/1FiOUvSmCVvt_TCpqX5mWlUF2UfH5V3_r/
>
> Please also check out this tutorial video for an in depth description of the strategy components: https://drive.google.com/file/d/1bgGH9qutoTcEwJtrGJMnJtYuEbokLjCZ/

For detailed documentation, visit https://algo.apifiny.com/user_guide/quantlib_configuration/. Within the json files, you can select and configure the strategy components. In general, Variables do computations with market data, Pricing Models fetch the data, Samplers determine when to fetch new data, and Strategies take in a signal and then decide how to trade based on it. Our QuantLib contains a range of pre-built and customizable modules to choose from for each strategy component.  

### Running your strategy on your local computer

> Please also check out this tutorial video for step by step instructions: https://drive.google.com/file/d/1aF6GFg_Q1u0Nm1yL_aQ5a4k3ptOlP-XR

In general, you can run your strategy on the terminal with the command ```ccc_sim_trader [path to config file] [date]```, where the date is in the YYYYMMDD format. This applies if you haven't compiled your own C++ code; if you have, see below. On Jupyter, you can bring up a terminal by going to File > New > Terminal. Remember to save your json file before running the strategy.

As an example, first go into the the /home/dev folder:

To run your strategy over one date, run this command, updating the date in YYYYMMDD format:
   ```
   ccc_sim_trader examples/maker.json 20200630
   ```

To run your strategy over a date range, run this command, updating the dates in YYYYMMDD format:
   ```
   gen_dates.py -sd 20200501 -ed 20200630 | parallel -j 64 ccc_sim_trader examples/maker.json
   ```

To get summary statistics of how your strategy did, run this command, updating the dates in YYYYMMDD format:
   ```
   sim_ana.py -sd 20200501 -ed 20200630 -p logs
   ```

In the summary, the pnlUSD is the profit before fees, and netUSD is the profit after fees.

On Jupyter, another way to run see the results of your strategy is running the Base.ipynb file located in the root folder (assigned on Jupyter to /home/dev). You can edit it by double-clicking on this file in the left file directory. Update the dates in the "DECLARE YOUR DATE RANGE" section, save it, then run it by pressing the >> button on the top of the tab. This method will give you a chart showing your cumulative PnL over time as well. 

>**To run your strategy using SWAP data instead,** replace maker.json with maker-swap.json and update the dates in the commands above. The SPOT data is from 2020-05-01 to 2020-06-30 and the SWAP data is from 2020-07-15 to 2020-09-14.

### C++ code

In the examples folder, you will find some C++ files. You can edit them and add your own. Once you have done so, to run your strategy:

1. in CCMain.cpp, import your strategy file.

2. In getStrategy, add a new condition to the if statement, with your strategy name in place of the default.

3. Build your strategy, by running in your base directory ```sh build_scripts/rebuild.sh```

4. Run your strategy using the same syntax as in the above section, but replace all instances of ```ccc_sim_trader``` with ```strat```.

## Submission

> Please also check out this tutorial video for step by step instructions: https://drive.google.com/file/d/1y2jWhTdD-nUizznJL3sZILlDhE1oHMpY

1. **Update 10/18: The deadline for submission is 9AM EST Thursday, 10/20. **

2. Please complete this form as part of your submission: https://share.hsforms.com/1Z6dIt7jERWeDBWYLbEtfaQc7zf0

3. Copy your configuration file and your C++ code (if applicable) into the submission folder on your own computer. First, go to the folder where the files located, then run the following command, using the naming convention below: ```cp [source file name] /home/dev/submission/[file name using naming convention below]```. 

   For example, to copy the maker.json file, run this command from the examples folder, updating userid with your own:
   ```
   cp maker.json /home/dev/submission/userid.json
   ```

   >Naming convention: Config files are to be named "[userid].json". C++ strategy files are to be named "[userid]-strat-[CLASS_NAME].cpp" and "[userid]-strat-[CLASS_NAME].h". Exactly one config file and one C++ strategy file (if applicable) can be submitted per competitor. If multiple are submitted, the most recently edited one(s) will be selected. If you wish to submit further C++ files (e.g. variables), make a folder within the "submission" folder called "xlibs" and place the compiled binary files there. 

4. Run the following command on the terminal on your own computer, replacing "userid" with your own: 
   ```
   rsync -a -e "ssh -p 2023" /home/dev/submission/*.* userid@38.142.207.130:~/cornell-trading-competition/submission/
   ```
5. Open a new terminal and log onto our server, using the instructions in the "Using our server" section above. Go into the ~/cornell-trading-competition/submission folder to confirm your submitted files are there.

6. Congrats on completing the competition! Winners will be announced on the day of the event on Saturday, 10/22. Good luck!

7. Prizes:
   
   1st Prize: $1000
   
   2nd Prize: $500
   
   3rd Prize: $250

## Sample code



    "variables": [
        ["FXRLIEN.TOUCOIN_trend30", ["Trend", {"pm": "FXRLIEN.TOUCOIN_midpx", "sampler": "TrendSampler"}]],

        
        ["MyVarEMA_00_60", ["Sub", {"v1": "MidPriceVar", "v2": "MyVarEma60"}]],
        ["MyVarEMA_00_30", ["Sub", {"v1": "MidPriceVar", "v2": "MyVarEma30"}]],
        ["MyVarEMA_00_01s", ["Sub", {"v1": "MidPriceVar", "v2": "MyVarEma01s"}]],

        ["MyVarEma01s", ["VarEma", {"variable": "MidPriceVar", "sampler": "h01sSampler"}]],
        ["MyVarEma05s", ["VarEma", {"variable": "MidPriceVar", "sampler": "h05sSampler"}]],
        ["MyVarEma30", ["VarEma", {"variable": "MidPriceVar", "sampler": "h30Sampler"}]],
        ["MyVarEma60", ["VarEma", {"variable": "MidPriceVar", "sampler": "h60Sampler"}]],

        
        ["MidPriceVar",["PriceVar", {"pm": "FXRLIEN.TOUCOIN_midpx"}]],
        ["BidPriceVar",["PriceVar", {"pm": "FXRLIEN.TOUCOIN_bidpx"}]],
        ["AskPriceVar",["PriceVar", {"pm": "FXRLIEN.TOUCOIN_askpx"}]]
       
    ],
    "samplers":[
        ["TrendSampler", ["TimeSampler", {"msecs": 1000, "halflife": 1800}]],

        
        ["h01sSampler", ["TimeSampler", {"msecs": 1000, "halflife": 1}]],
        ["h05sSampler", ["TimeSampler", {"msecs": 1000, "halflife": 5}]],
        ["h30Sampler", ["TimeSampler", {"msecs": 1000, "halflife": 1800}]],
        ["h60Sampler", ["TimeSampler", {"msecs": 1000, "halflife": 3600}]]

    ],
    "pricing_models":[
        ["FXRLIEN.TOUCOIN_midpx", ["MidPx", {"port": ["FXRLIEN", "TOUCOIN"]}]],
        ["FXRLIEN.TOUCOIN_bidpx", ["BidPx", {"port": ["FXRLIEN", "TOUCOIN"]}]],
        ["FXRLIEN.TOUCOIN_askpx", ["AskPx", {"port": ["FXRLIEN", "TOUCOIN"]}]]
    ],
    "models":[
        ["FXRLIENTOUCOIN_model", ["LinearModel", {"variable": "MyVarEMA_00_60"}]]
    ],
    "strategies": [                                 
        ["Buy", [
            "CCSimpleMakerStrategy", {
                "symbol": "FXRLIEN",
                "trade_market": "TOUCOIN",
                "make_mid_pm": "FXRLIEN.TOUCOIN_askpx",
                "account": 101,
                "model": "FXRLIENTOUCOIN_model",
                "use_bps_thold": true,
                "make_thold": 3,
                "order_notional": 100
                }
            ]
        ],                          
        ["Sell", [
            "CCSimpleMakerStrategy", {
                "symbol": "FXRLIEN",
                "trade_market": "TOUCOIN",
                "make_mid_pm": "FXRLIEN.TOUCOIN_bidpx",
                "account": 101,
                "model": "FXRLIENTOUCOIN_model",
                "use_bps_thold": true,
                "make_thold": 3,
               "order_notional": 100
                }
            ]
        ]                          
    ]
      }

## Final Rankings

Congrats to the winners and all those who participated! Please stay tuned on our social media channels for more exciting new features rolling out for all our products.

| Rank | Team Leader | University | Prize |
| ------ | ------ | ------ | ------ |
|    1   | Matthew Li | Carnegie Mellon University, University of Illinois Urbana Champaign     |  $1000  |
|    2   | Andrew Cheng  | Cornell University | $500   |
|    3   | Alex Ni   | Cornell University   |  $250  |


For the complete list of results, please visit: 

https://docs.google.com/document/d/1nZIHvHT6KnxluMmv-Zx0hSonwq0xhRQwvIYucIM_cuA/
